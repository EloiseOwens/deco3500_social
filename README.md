# deco3500_social

This is the README.md file for Team Social's prototype. 

Key links 
-------------------------------------------------------------------------
Documentation: https://gitlab.com/EloiseOwens/deco3500_social.wiki.git

Web-version of prototype: https://xd.adobe.com/view/2fc3ef1e-9e89-4069-98e4-936152e48e06-7a90/?fbclid=IwAR0sFY6LGVi-qpug-TMmsDJWGzQglK92CXokNWbo2grkeDPPX_tDvS3ycgw&fullscreen

Supporting materials including video, brochure and poster are all embedded in the design process overview page of the wiki: https://gitlab.com/EloiseOwens/deco3500_social/-/wikis/1.-Design-Process-Overview AND are available as PDFs in our git repository. 


Documentation
-----------------------------------------------------------------------------
All documentation is in the Wiki, with the main/design process overview page being: https://gitlab.com/EloiseOwens/deco3500_social/-/wikis/1.-Design-Process-Overview. 

The documentation is structured as a table of contents would be, i.e. 1., 1.1, 2, 2.2, etc. 
Existing solutions; interaction flow and prototypes; and personas and use cases are listen under the "Conceptual Design" header, as these were all components of the design process. 

The Wiki also features documentation regarding user testing and in-class stand-ups. 


Prototype
----------------------------------------------------------------------------
There is a link to the Adobe XD version of the prototype on the design process overview Wiki page.
This is a link to the web-version of the prototype: https://xd.adobe.com/view/2fc3ef1e-9e89-4069-98e4-936152e48e06-7a90/?fbclid=IwAR0GPDVEfWv6WEpy_bTaK9KSwZSr1oKulHKhsPl6SBAcunMlIGLjIteJB0c&fullscreen.

The login/sign up information is prefilled to simluate a real login process. 

Our two key features are the "social reading" and "book club" features. 


An overview/instrucitons of the social reading feature:

-	From the home page access the social reading tab from navigation bar.
-	Select ‘Your Books’ 
-	From here the user has the option to view all the books in their library or just the ones they have not started 
-	Select a book my clicking on the book icon
-	From the book page the user can: 
	o	Hide comments 
	o	View comments 
	o	Add comments 
	o	Reply to comments 
	o	Like comments 
-	Go back onto the social reading home page and select ‘friend’s books’ 
-	From here the user can view what books their friends are reading 
-	Select a book
-	The user can view the description of the book and then choose to purchase the book or add to their wish list. 


An overview/instructions of the book club feature:
-	From the home page access the book club tab from the navigation bar 
-	From here the user can view the public/ private book clubs they are in, recommended / friends book clubs and the option to create a book club 
-	Select a book club from ‘your public/ private book clubs’
-	From here the user can 
	o	View the book by clicking on the book icon
	o	Open the book club meeting 
	o	View members of the book club 
	o	Choose to leave the book club 
-	Select ‘open’ next to the meeting time. This will allow the user to view the book club meeting chat room. 

